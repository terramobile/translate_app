package io.github.vex8.terratranslate;

import java.io.*;
import javax.swing.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class Items extends DefaultListModel{

	LinkedHashMap<String, String> items;
	public Items(){
		super();
		items = new LinkedHashMap<String, String>();
		// addElement();
	}

	public void addItem(String key, String value){
		addElement(key);
		setItem(key, value);
	}

	public void setItem(String key, String value){
		items.put(key, value);
	}

	public String getItem(String key){
		return items.get(key);
	}

	public String getItemAt(int index){
		return (String) elementAt(index);
	}

	public String getFinal(){
		String returnValue = "";
		for(Map.Entry<String, String> pair : items.entrySet()){
			returnValue += pair.getKey()+"="+pair.getValue()+"\n";
		}
		return returnValue;
	}
}
