package io.github.vex8.terratranslate;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.file.*;

public class TranslateWindow extends JFrame{
	JPanel mainPanel;
	Items items;
	JList itemsDisplay;
	JScrollPane itemsScroller;
	JMenuBar menubar;
	JTextArea translation;
	JLabel translationLabel;
	final JFileChooser fc;

	private String value;
	private String current;

	public TranslateWindow(String title, int width, int height){
		super(title);
		// setSize(width, height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());

		fc = new JFileChooser();

		mainPanel = new JPanel(new GridBagLayout());
		// mainPanel.setPreferredSize(new Dimension(width, height));

		createLayout();
		run();
	}

	void run(){
		add(mainPanel, BorderLayout.CENTER);
		pack();
		setVisible(true);
	}

	void createLayout(){
		createMenuBar();

		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.BOTH;
		c.weighty = 1;
		c.weightx = 1;
		c.anchor = GridBagConstraints.CENTER;

		items = new Items();
		itemsDisplay = new JList(items);
		itemsDisplay.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		itemsDisplay.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e){
				// if(e.getValueIsAdjusting()) return;
				current = itemsDisplay.getSelectedValue().toString();
				value = items.getItem(current);
				value = value.replace("\n", "\n");
				translation.setText(value);
			}
		});
		itemsScroller = new JScrollPane(itemsDisplay);

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 4;

		mainPanel.add(itemsScroller, c);

		translationLabel = new JLabel("Text:");

		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 2;
		c.gridheight = 1;

		mainPanel.add(translationLabel, c);

		translation = new JTextArea(5, 20);
		translation.setLineWrap(true);
		translation.setWrapStyleWord(true);
		JScrollPane textscroll = new JScrollPane(translation);

		c.gridy = 1;
		c.gridheight = 2;

		mainPanel.add(textscroll, c);

		JPanel buttonPanel = new JPanel();
		JButton ok = new JButton("Save Changes");
		ok.setActionCommand("savechanges");
		ok.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if("savechanges".equals(e.getActionCommand())){
					value = translation.getText();
					items.setItem(current, value);
				}
			}
		});

		buttonPanel.add(ok);

		c.gridy = 3;
		c.gridheight = 1;

		mainPanel.add(buttonPanel, c);
	}

	void createMenuBar(){
		menubar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		// file.setMnemonic();

		JMenuItem openMenuItem = new JMenuItem("Open");
		openMenuItem.setToolTipText("Open a terraria mobile locale file");
		openMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				// TODO: load files here
				int returnVal = fc.showOpenDialog(TranslateWindow.this);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					File file = fc.getSelectedFile();

					try(BufferedReader reader = Files.newBufferedReader(file.toPath())){
						String line = null;
						String[] pair = null;
						while((line = reader.readLine()) != null){
							 pair = line.split("=");
							 if(pair.length == 1) items.addItem(pair[0], "");
							 else items.addItem(pair[0], pair[1]);
						}
					}
					catch(IOException e){
						e.printStackTrace();
					}
				}
			}
		});

		JMenuItem saveMenuItem = new JMenuItem("Save");
		saveMenuItem.setToolTipText("Save modified translation");
		saveMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				// TODO: save files here
				int returnVal = fc.showSaveDialog(TranslateWindow.this);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					File file = fc.getSelectedFile();
					String save = items.getFinal();

					try(BufferedWriter writer = Files.newBufferedWriter(file.toPath())){
						writer.write(save);
					}
					catch(IOException e){
						e.printStackTrace();
					}
				}
			}
		});

		fileMenu.add(openMenuItem);
		fileMenu.add(saveMenuItem);
		menubar.add(fileMenu);

		setJMenuBar(menubar);
	}
}
